# Practical Computing for Biologists SP22

This repository contains the source code, datasets, and documentation for 
Dan Barshis' Practical Computing for Biologists course (Biology 701/801) at Old Dominion University, as 
taught in the Spring of 2022.

See the [syllabus](https://bitbucket.org/dbarshis/22sp_pcfb/src/master/syllabus/Biol701-801_PCB_Spring_2022_Barshis.pdf) 
for additional information.

The book for the class is:

Haddock, S. H. D. and Dunn, C. W. (2010). Practical Computing for Biologists. 
[Sinauer Associates](http://practicalcomputing.org). Required but scanned copies available.

Here are some of the appendices from the book, which summarize frequently used 
commands:
[Appendices](http://practicalcomputing.org/files/PCfB_Appendices.pdf)

## Pre-class to-do's

  - Install the software mentioned below

## Assignments

- Will be posted under Assignments folder

## Software to install on your own computer

A laptop will be required for all in-class sessions. You will use the following programs
to work with data on your laptop.

### For OS X (ie, Macs):

- [BBedit](https://www.barebones.com/products/bbedit/)

### For Microsoft Windows:

- [Notepad++](https://notepad-plus-plus.org/)

- Scroll down and follow instructions to install [TheBashShell for windows](https://carpentries.github.io/workshop-template/) and [Python](https://carpentries.github.io/workshop-template/)

##External links

[software-carpentry](http://software-carpentry.org) has some good tutorials and resources
though they are geared more towards two day intensive workshops

[evomics](http://evomics.org/learning/unix-tutorial/) a really good unix tutorial with a slant towards NGS bioinformatics

## Licenses

All original software is licensed under a 
[GPL v3 license](http://www.gnu.org/licenses/gpl-3.0.html). 
Data that is downloaded from public archives is distributed in accordance with 
the license of the source archive. All other original content is distributed 
under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported 
License](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US).
