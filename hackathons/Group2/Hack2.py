#! /usr/bin/env python

Usage = """
Read multiple CBASS data files
Remove any experiments with PrintDate "Testing_2019"
Remove any experiments that took place July 15th - August 4th (These data are mislabelled tests and not experiments)
Remove any extra header lines in both files so there's only one header at the top
Remove “TempT*” and “T*RelayState” columns

Separating: 
Put “KL_2020_EXPERIMENT” experiments into a separate file
Combine “COVID_CRUISE_2020” experiments to single file 

Refining: 
Combine Th, Tm, Ts, columns to “HH:MM:SS” for time 
Rename PrintDate “COVID_CRUISE_2020” -> “2020-08-21_NSU_Acer_Controller01” (or “Controller02” depending on file origin)
Add a column with the calculated difference between the temperature setpoint and temperature reading (T*SP - T*inT)

PrintDate,Date,N_ms,Th,Tm,Ts,T1SP,T1inT,TempT1,T1RelayState,T2SP,T2inT,TempT2,T2RelayState,T3SP,T3inT,TempT3,T3RelayState,T4SP,T4inT,TempT4,T4RelayState,

"PrintDate,Date,Time,T1SP,T1inT,T1diff,T2SP,T2inT,T2diff,T3SP,T3inT,T3diff,T4SP,T4inT,T4diff,"
"""


import sys, time

starttime = time.time()

def padmin(Min):
	if len(Min)==1:
		OutMin='0'+Min
	else:
		OutMin=Min
	return OutMin
    
BadDateList = []
for date in list(range(15,32,1)):
    BadDateList.append("2020_July_" + str(date))
for date in list(range(1,5,1)):
    BadDateList.append("2020_August_" + str(date))
                    
if len(sys.argv)<2:
	print(Usage) #print usage statements to the screen
else:
    FileList = sys.argv[1:]
    FileNumber = 0
    for InFileName in FileList:
        OutFileName1 = "2020-08_NSU_Acer.csv"
        OutFileName2 = "2020-07_Shedd_Apal.csv"
        
        InFile = open(InFileName, 'r')
        OutFile1 = open(OutFileName1, 'w')
        OutFile2 = open(OutFileName2, 'w')

        Headerline = "PrintDate,Date,Time,T1SP,T1inT,T1diff,T2SP,T2inT,T2diff,T3SP,T3inT,T3diff,T4SP,T4inT,T4diff,"
        
        if FileNumber == 0:
            OutFile1.write(Headerline + '\n')
            OutFile2.write(Headerline + '\n')
        LineNumber = 0
        for Line in InFile:
            if Line.startswith("PrintDate") or Line.startswith("Testing") or Line.startswith(","):
                continue
            else:
                Line = Line.strip("\n").strip("\r")
                List = Line.split(',')
                PrintDate = List[0]
                Date = List[1]
                Th = List[3]
                Tm = padmin(List[4])
                Ts = List[5]
                T1SP = List[6]
                T1inT = List[7]
                T2SP = List[10]
                T2inT = List[11]
                T3SP = List[14]
                T3inT = List[15]
                T4SP = List[18]
                T4inT = List[19]
                
                Time = "%s:%s:%s" %(List[3], List[4], List[5])
                
                T1diff = str(round(float(T1SP) - float(T1inT), 2))
                T2diff = str(round(float(T2SP) - float(T2inT), 2))
                T3diff = str(round(float(T3SP) - float(T3inT), 2))
                T4diff = str(round(float(T4SP) - float(T4inT), 2))
                
                DateSplit = Date.split('_')
                Year = DateSplit[0]
                Month = DateSplit[1]
                Day = DateSplit[2]
                
                if Date in BadDateList:
                    continue
                else: 
                    if Month == "July":
                        Month = "07"
                    if Month == "August":
                        Month = "08"
                        
                        
                    if PrintDate == "COVID_Cruise_2020":
                        NewPrintDate = "%s-%s_NSU_Acer_%s" %(Year, Month, InFileName[:-4])
                        OutList1 = [NewPrintDate,Date,Time,T1SP,T1inT,T1diff,T2SP,T2inT,T2diff,T3SP,T3inT,T3diff,T4SP,T4inT,T4diff,]
                        OutFile1.write('%s\n' %(','.join(OutList1)))
                    if PrintDate == "KL_2020_EXPERIMENT":
                        NewPrintDate = "%s-%s_Shedd_Apal_%s" %(Year, Month, InFileName[:-4])
                        OutList2 = [NewPrintDate,Date,Time,T1SP,T1inT,T1diff,T2SP,T2inT,T2diff,T3SP,T3inT,T3diff,T4SP,T4inT,T4diff,]
                        OutFile2.write('%s\n' %(','.join(OutList2)))
            
            LineNumber+=1
        InFile.close()
        OutFile1.close()
        OutFile2.close()
print("this script took %f seconds" %(time.time()- starttime))
               
