#! /usr/bin/env python

Usage = '''
Read multiple CBASS data files
#Remove any experiments with PrintDate Testing_2019
Remove any experiments that took place July 15th - August 4th (These dates are mislabelled tests and not experiments)
Remove any extra header lines in both files so there's only one header at the top
Remove TempT and TRelayState columns

Separating:
Put "KL_2020_EXPERIMENT" experiments into a separate file
Combine "COVID_CRUISE_2020" experiments into a single file

Refining:
Combine Th, Tm, Ts columns to "HH:MM:SS" for time
Rename PrintDate "COVID_CRUISE_2020" -> "2020-08-21_NSU_Acer_Controller01" (or "Controller02" depending on file origin)
Add a column with the calculated difference between the temperature setpoint and temperature reading (T1SP-T1inT)
'''

import sys, time

StartTime=time.time()

#Function to calculate temperature differential
def calcdiff(TSP,TinT):
	Diff=str(round(float(TSP)-float(TinT),2))
	return Diff

#Function to add leading zeros to string of length 1
def padmin(Min):
	if len(Min)==1:
		OutMin='0'+Min
	else:
		OutMin=Min
	return OutMin

if len(sys.argv)<2:
	print(Usage)
else:
	Delimiter= ',' #change this if you want tab delimited output
	FileList=sys.argv[1:]
	FileNumber = 0
	Header = ['PrintDate','Date','Time','T1SP','T1inT','T1diff','T2SP','T2inT','T2diff','T3SP','T3inT','T3diff','T4SP','T4inT','T4diff']
	OutFile1=open('NSU_Acer_CovidCruise2020.csv','w')
	OutFile1.write('%s\n' %(','.join(Header)))
	OutFile2=open('Shedd_Apal_KL_2020_EXPERIMENT.csv','w')
	OutFile2.write('%s\n' %(','.join(Header)))
#Making list of dates to skip
	DatesToSkip={}
	for Num in range(15,32):
		DatesToSkip['2020_July_'+str(Num)]=''
	for Num in range(1,5):
		DatesToSkip['2020_August_'+str(Num)]=''
#now looping through files
	for InFileName in FileList:
		InFile = open(InFileName, 'r')
		LineCount = 0
		for Line in InFile:
			if Line.startswith('PrintDate') or Line.startswith('Testing_2019'):
				continue
			else:
				Line=Line.strip('\n').strip('\r')
				Cols=Line.split(',')
				try:
					DatesToSkip[Cols[1]]
				except KeyError:
					YearMoDay=Cols[1].split('_')
					if YearMoDay[1] == 'July':
						Month = '07'
					if YearMoDay[1] == 'August':
						Month = '08'
					if Cols[0] == 'COVID_Cruise_2020':
						OutList=['%s-%s_NSU_Acer_%s' %(YearMoDay[0],Month,InFileName[:-4]),Cols[1],'%s:%s:%s' %(Cols[3],padmin(Cols[4]),Cols[5]),Cols[6],Cols[7],calcdiff(Cols[6],Cols[7]),Cols[10],Cols[11],calcdiff(Cols[10],Cols[11]),Cols[14],Cols[15],calcdiff(Cols[14],Cols[15]),Cols[18],Cols[19],calcdiff(Cols[18],Cols[19])]
						OutFile1.write('%s\n' %(','.join(OutList)))
					elif Cols[0] == 'KL_2020_EXPERIMENT':
						OutList=['%s-%s_Shedd_Apal_%s' %(YearMoDay[0],Month,InFileName[:-4]),Cols[1],'%s:%s:%s' %(Cols[3],padmin(Cols[4]),Cols[5]),Cols[6],Cols[7],calcdiff(Cols[6],Cols[7]),Cols[10],Cols[11],calcdiff(Cols[10],Cols[11]),Cols[14],Cols[15],calcdiff(Cols[14],Cols[15]),Cols[18],Cols[19],calcdiff(Cols[18],Cols[19])]
						OutFile2.write('%s\n' %(','.join(OutList)))
			LineCount += 1
		InFile.close()
	OutFile1.close()
	OutFile2.close()
sys.stderr.write('File processing took %f seconds\n' %(time.time()-StartTime))