#! /usr/bin/env python

Usage = """
Hack1.py - version 1.0
Hackathon 1 script that changes format of ctd bottle files and averages temps and salinities.
Exports reorganized files into individual .csv files and a merged .csv file with all data
Usage:
	Hack1.py *.btl
"""

# import modules we want

import re # regular expressions
import sys, time
StartTime = time.time()



if len(sys.argv)<2:
	print(Usage) #print usage statements to the screen
else:
	FileList=sys.argv[1:]
	FileNumber = 0
	CombinedOutFileName = "MergedCruiseCTD.csv"
	CombinedOutFile = open(CombinedOutFileName, 'w')
	for InFileName in FileList:
		InFile = open(InFileName, 'r') # open the infile

		OutFileName = InFileName[:-4]+".csv"
		OutFile = open(OutFileName, 'w') # create the outfile and open it

		# Define a headerline for outfile
		
		Headerline = "Ship_name,Station_name,Station_no,Bottle,Date(mmddyyyy),Time,Lat,Lon,Depth,Temp_av,Sal_av,wet_cDOM,FEICO-AFL,OxSol_Mm_kg,Density00"
		if FileNumber == 0:
			CombinedOutFile.write(Headerline + '\n')
		OutFile.write(Headerline + '\n')

		# Initialize the counter used to keep track of the line
		LineNumber = 0

		for Line in InFile:
			Line = Line.strip('\n').strip('\r')
			if Line.startswith("** S"):
				if "Ship" in Line:
					ShipName = Line.split(": ")[1]
				if "Station Number" in Line:
					StationNumber = Line.split(": ")[1]
				if "Station Name" in Line:
					StationName = Line.split(": ")[1]
		
			elif Line.endswith("(avg)"):
	
				SearchString = '^\s+(\d+)\s+(\w+)\s(\d+)\s(\d+)\s+([\d\.]+)\s+([\-\d\.]+)\s+([\-\d\.]+)\s+[\d\.]+\s+([\-\d\.]+)\s+([\-\d\.]+)\s+[\-\d\.]+\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s+[\d\.]+\s+([\-\d\.]+)\s+([\-\d\.]+)\s+[\-\d\.]+\s+([\-\d\.]+).+'
	
				Result = re.search(SearchString,Line)
	
				Bottle = Result.group(1)
				Month = Result.group(2)
				Day = Result.group(3)
				Year = Result.group(4)
				Depth = Result.group(5)
				Lat = Result.group(6)
				Lon = Result.group(7)
				T0 = float(Result.group(8))
				T90 = float(Result.group(9))
				Sal0 = float(Result.group(10))
				Sal1 = float(Result.group(11))
				Density = Result.group(12)
				ChlA = Result.group(13)
				CDOM = Result.group(14)
				SolOX = Result.group(15)
				AveSal = (Sal1 + Sal0)/2
				AvTemp = (T0+T90)/2
		
				# change date format
				if Month == "Sep":
					Month = "09"
				if Month == "Aug":
					Month = "08"
		
			elif Line.endswith("(sdev)"):
				SearchString2 = '^\s+([\:\d]+).+'
				Result2 = re.search(SearchString2,Line)
				Time = Result2.group(1)
	
			# Writing new line to outfile
				OutList=[ShipName, StationName, StationNumber, Bottle, Month+Day+Year, Time, Lat, Lon, Depth,'%.3f'%(AvTemp), '%.3f'%(AveSal), CDOM, ChlA, SolOX, Density]
				OutFile.write('%s\n' %(','.join(OutList)))
				CombinedOutFile.write('%s\n' %(','.join(OutList)))
	
			LineNumber+=1

		InFile.close()
		OutFile.close()
		FileNumber+=1
	CombinedOutFile.close()

print("File Processing Took %f seconds" %(time.time() - StartTime))