#! /usr/bin/env python

import sys, time, argparse
from datetime import datetime

def main():
	#to parse command line
	Usage = """
22Sp_Group1_djb.py - version 1.0
Hackathon group 1 script
Parse/wrangle a series of ctd .btl files
to comma delimited text files including only the following data
'Ship_name','Station_name','Station_no','Date','Time','Lat','Lon','Depth','Temp_av','Sal_av','wet_cDOM','FEICO-AFL','OxSol_Mm_kg','Density00'
with the parsed outputfile names as inputfile_processed.csv 
Usage:
	22Sp_Group1_djb.py -i *.btl
"""

	p = argparse.ArgumentParser(Usage)


	#Input/output files
	p.add_argument('-i', '--infilelist', nargs='+', help='*.btl files to parse')
	p.add_argument('-o', '--outfilesuffix', default='_processed', help='New, processed, bottle.csv file [default _processed]')
	p.add_argument('-d', '--delimiter', default=',', help='output file delimiter [default ,]')
	p.add_argument('-x', '--outfileextension', default='.csv', help='output file extension [default .csv]')

	#General
	args = p.parse_args()

	StartTime=time.time()
	FileList=args.infilelist
	OutFileSuffix=args.outfilesuffix
	if args.delimiter=='tab':
		Delimiter='\t'
	else:
		Delimiter=args.delimiter
	OutFileExtension=args.outfileextension
	CombinedOutFileName='MergedCruiseData'
	CombinedOutFile=open('%s%s' %(CombinedOutFileName, OutFileExtension), 'w')
	FileCount=0
	HeaderList=['Ship_name','Station_name','Station_no','BottlePos','Date','Time','Lat','Lon','Depth','Temp_av','Sal_av','wet_cDOM','FEICO-AFL','OxSol_Mm_kg','Density00']
	CombinedOutFile.write('%s\n'%(Delimiter.join(HeaderList)))
	for InFileName in FileList:
		InFile=open(InFileName, 'r')
		OutFile=open('%s%s%s'%(InFileName[:-4],OutFileSuffix,OutFileExtension),'w')
		OutFile.write('%s\n'%(Delimiter.join(HeaderList)))
		LineCount=-1
		for Line in InFile:
			LineCount+=1
			Line=Line.strip('\n').strip('\r')
			if Line.startswith('** S'):
				Stuffs = Line.split(': ')
				if 'Ship' in Line:
					Ship=Stuffs[1]
				if 'Station Number' in Line:
					StNum=Stuffs[1]
				if 'Station Name' in Line:
					StName=Stuffs[1]
			elif Line.startswith('      '):
				if 'avg' in Line:
					DataList=list(filter(None, Line.split(' ')))
					BottlePos=DataList[0]
					DateString='%s %s %s' %(DataList[1],DataList[2], DataList[3])
					Day=DataList[2]
					Month=str(datetime.strptime(DateString, '%b %d %Y').date())[5:7]
					Year=DataList[3]
					Lat=DataList[5]
					Lon=DataList[6]
					Depth=DataList[4]
					Temp_av=(float(DataList[8])+float(DataList[9]))/2
					Sal_av=(float(DataList[11])+float(DataList[12]))/2
					wet_cDOM=DataList[16]
					FEICOAFL=DataList[15]
					OxSol_Mm_kg=DataList[18]
					Density00=DataList[13]
				if 'sdev' in Line:
					Time=list(filter(None, Line.split(' ')))[0]
					OutList=[Ship, StName, StNum, BottlePos, Month+Day+Year, Time, Lat, Lon, Depth, '%.3f'%(Temp_av), '%.3f'%(Sal_av), wet_cDOM, FEICOAFL, OxSol_Mm_kg, Density00]
					OutFile.write('%s\n' %(Delimiter.join(OutList)))
					CombinedOutFile.write('%s\n' %(Delimiter.join(OutList)))
		FileCount+=1
		sys.stderr.write('Finished processing file #%d: %s\n'%(FileCount, InFileName))
		InFile.close()
		OutFile.close()
	sys.stderr.write('File processing took %f seconds\n'%(time.time()-StartTime))

if __name__ == '__main__':
	main()